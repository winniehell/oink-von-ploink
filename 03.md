# 3

Zwei Löcher – das war unglaublich.
Herr Oink von Ploink stand vor "seinem" Loch.
In der Nacht hatte es geregnet und das Loch stand inzwischen eine handbreit voll mit Wasser.
Die wütenden Kommentare um ihn herum waren ihm egal.
Auch die Spritzer der Leute, die das Loch zu spät erkannten und in die Pfütze traten, störten ihn nicht.
Was ihn störte war, dass dieses Loch jetzt seit zwei Wochen immer größer wurde und sich niemand darum kümmerte.
Niemand außer ihm.

Als er das zweite Loch vor dem Restaurant entdeckt hatte, war er noch einmal hinein gegangen und hatte den Kellner darauf angesprochen.
Zunächst verstand dieser nicht, was das Problem war.
Schließlich konnte Herr Oink von Ploink ihn überzeugen, mit vor die Tür zu kommen, damit sie sich das Loch zusammen ansehen konnten.
Der Kellner warf einen Blick auf das Loch im Bürgersteig vor dem Restaurant, ließ dann seinen Frack durch ein Schulterzucken wackeln und ging zurück ins Restaurant mit dem Kommentar:

- "Die Abteilung für Straßenbau wird sich schon darum kümmern.
   Einen schönen Abend noch!"
- "Abteilung für Straßenausbesserung!"

zischte Herr Oink von Ploink hinterher.
Dann wollte er noch ein unfreundliches Wort ergänzen, doch

- "Sie... Kellner!"

war alles was ihm einfiel.

Den wütenden Straßenmonolog hatte fast keiner bemerkt.
Als Herr Oink von Ploink sich erneut zum Gehen wandte, vorbei an Loch Nummer 2, bemerkte er, dass er beobachtet wurde.
Auf der anderen Seite der Straße, saß ein kleiner Hund.
Er trug kein Halsband und sah so aus, als könnte sich sein Fell nicht mehr an das letzte Bad erinnern.

Herr Oink von Ploink beschleunigte.
Er hatte schon ein Lochproblem.
Jetzt wollte er nicht auch noch ein Hundproblem dazu.
Dann blieb er stehen.
Langsam drehte er sich um.
Der Hund sah ihn an, folgte ihm aber nicht.
Es fing an zu tröpfeln.
Herr Oink von Ploink überlegte kurz, dann sagte er zu dem Hund:

- "Komm mit!
   Hier draußen ist es kalt und nass.
   Heute kannst du bei mir bleiben."

Der Hund schien zu verstehen und folgte in einem gewissen Abstand.
Er ging mit Herrn Oink von Ploink zusammen die Treppe zur Wohnung hoch.
Drinnen schüttelte er sich den Regen aus dem Fell.
Die Kommode im Flur bekam ein neues Punktemuster aus Matsch.

Herr Oink von Ploink bestellte zwei Portionen Hundefutter.
Dann schnappte er sich Herkules und setzte ihn in die Badewanne.
Den Namen hatte er in irgendeiner alten Geschichte schon einmal gehört.
Seine Oma liebte alte Geschichten.
Es war irgendein hochrangiger Sachbearbeiter, der überdurchschnittlich gearbeitet hatte, wenn sich Herr Oink von Ploink richtig erinnerte.
Das schien ihm genau der richtige Name für diesen Hund.
Gemeinsam mit Herkules würde er das Lochproblem aus der Welt schaffen.

Herkules ließ das Bad über sich ergehen ohne sich zu wehren.
Sogar das Shampoo mit Provitamin B5 und Birnbaumessenz nahm er hin.
Beim Föhnen guckte er ein wenig skeptisch.
Das frisch gelieferte Hundefutter schien es wieder gut zu machen.
Beide Portionen waren innerhalb von wenigen Minuten vernichtet.
Danach rollte Herkules sich auf dem Sofa zusammen.

"Hundehaltung ist strengstens untersagt", stand im Mietvertrag.
Doch bei einer Übernachtung konnte man ja wohl nicht von Hundehaltung sprechen, dachte sich Herr Oink von Ploink.
Es war eher eine Art Hundebesuch.
Inzwischen war auch er müde geworden und nachdem er zwei weitere Hundefutterportionen für das Frühstück bestellt und die Schüssel mit Wasser aufgefüllt hatte, legte sich Herr Oink von Ploink ins Bett.

Im Büro kamen Herrn Oink von Ploink jetzt Zweifel, ob es eine gute Idee gewesen war, Herkules alleine in der Wohnung zu lassen.
Er hatte dafür gesorgt, dass genug Futter und Wasser für den Hund vorhanden waren und die beiden waren auch am frühen Morgen nach draußen gegangen, um Hundegeschäfte abzuwickeln.
Doch was wenn der Hund anfing, die Wohnung zu verwüsten?
Oder plötzlich Panik bekam und nach draußen wollte?
Wahrscheinlich war es das beste, in der Mittagspause schnell nach dem Hund zu sehen.

Als Herr Oink von Ploink mittags die Wohnungstür öffnete, erwartete Herkules ihn schon.
Das Hundefutter war bis zum letzten Krümel verschwunden und der Wasserstand in der Schüssel war gesunken.
Vielleicht war es das beste, Herkules mit zur Arbeit zu nehmen, überlegte Herr Oink von Ploink.
Es gab dort einen überdachten Innenhof mit Bänken und Pflanzen, in dem sich fast nie jemand aufhielt.
Von der Kaffeemaschine aus konnte man den Hof überblicken.

Herr Oink von Ploink schnappte sich schnell eine Decke und den Hund, bestellte weiteres Hundefutter ins Büro und machte sich auf den Rückweg zu seinem Arbeitsplatz.
Unterwegs kam er an Loch Nummer 1 vorbei, auf dem inzwischen ein paar bunte Blätter schwammen.
Ob sich wohl Fische dort ansiedelten, wenn es nur oft genug regnete?

Kurz vor Ende der Mittagspause, lag Herkules auf der Decke neben einer Bank im Innenhof, ausgestattet mit Hundefutter und sogar Herr Oink von Ploink hatte es geschafft eine Kleinigkeit zu essen.
Auf dem Weg zu seinem Schreibtisch kam er an Gina vorbei, die ihn nicht weiter beachtete.
Ach, dachte er sich, das wäre bestimmt nicht lange gut gegangen.

Nach der Arbeit, ging Herr Oink von Ploink in den Innenhof, um Herkules abzuholen.
Zwischendurch hatte er immer wieder aus dem Fenster gesehen.
Der Hund blieb brav auf seiner Decke liegen.
Jetzt stand er auf und blickte Herrn Oink von Ploink erwartungsvoll an.

- "Komm, wir gehen Oma besuchen."

sagte dieser.

Oma freute sich über den überraschenden Besuch.
Sie freute sich auch über Herkules, denn sie mochte Tiere.

- "Ich weiß noch nicht, was ich mit ihm machen soll.
   Bei mir kann er nicht bleiben, der Vermieter erlaubt keine Hunde.
   Zurück auf die Straße setzen möchte ich ihn auch nicht."
- "Er kann bei mir bleiben."

schlug Oma vor.

- "Hier dürfen wir Haustiere haben.
   Einige meiner Nachbarn haben auch Hunde und ich könnte ein wenig Gesellschaft gut gebrauchen."

Damit war es beschlossene Sache.
Herr Oink von Ploink registrierte Oma für ein Hundefutterabo und versprach öfter vorbei zu kommen, um nach den beiden zu sehen.

Dann erzählte er Oma von dem zweiten Loch.
Das Erlebnis mit Gina verschwieg er.

- "Eigenartig... sonst ist doch immer alles geregelt."

meinte Oma nachdenklich.

- "Vielleicht hat die Abteilung für Straßenausbesserung zu viel zu tun und kann sich nicht mehr um alle Löcher kümmern."

schlug Herr Oink von Ploink vor.

- "Warst du denn schon einmal dort, Oskar?"
- "Nein, mein Vorgesetzter hat mein VERBOT nicht unterschrieben.
   Ich habe auch bei der Servicehotline angerufen, habe aber niemanden erreicht."
- "Ach, Servicehotlines..."

Oma schüttelte den Kopf.

- "Dann machen Herkules und ich wohl morgen einen Ausflug.
   Mit der S-Bahn kommt man heutzutage ja eigentlich überall bequem hin."

Herr Oink von Ploink lächelte.
Es freute ihn, dass Oma einen Grund gefunden hatte, das Haus zu verlassen.
Und noch mehr freute es ihn, dass er jetzt eine Verbündete beim Kampf gegen das Lochproblem hatte.
Er verabschiedete sich von Herkules und Oma und ging zufrieden nach Hause.

Die erste Hälfte seiner Lieblingsserie hatte er verpasst, doch das machte ihm heute nichts aus.
Herr Müller stellte gerade einen neuartigen Duschvorhang mit verbesserter Formel für Wasserperleffekt vor.
Kurz darauf, war Herr Oink von Ploink auf dem Sofa eingeschlafen.

In dieser Nacht träumte er, dass er auf dem Weg zur Arbeit war und sich den überfüllten Bürgersteig mit vielen anderen Menschen teilte.
Plötzlich sah er ein Stück weiter vorne ein riesiges Loch im Boden.
Leute stürzten hinein und schafften es nicht mehr heraus zu klettern.
Er blieb sofort stehen und schrie:

- "Halt, geht nicht weiter!
   Da vorne ist ein Loch im Boden.
   Bleibt stehen!"

Doch die Masse bewegte sich unaufhaltsam vorwärts.
Er wurde angerempelt und beschimpft von Menschen, die zur Arbeit wollten.
Sie waren schon immer diesen Weg gegangen und sahen keinen Grund darin, ausgerechnet heute damit aufzuhören.
Alle fielen in das Loch.
Herr Oink von Ploink versuchte, einige von ihnen wieder herauszuziehen, doch er war zu schwach.

Schweißgebadet wachte er auf dem Sofa auf.
Der Fernseher lief noch.
Herr Müller stellte gerade eine innovative Knoblauchpresse vor.
Es war mitten in der Nacht und so beschloss Herr Oink von Ploink, den Fernseher auszuschalten und den Rest der Nacht in seinem Bett zu verbringen.
Als es ihm endlich gelang, einzuschlafen, kehrte er in unruhige Träume von Löchern im Boden zurück.

Geweckt vom durchdringenden Piepen seines Weckers, schreckte er auf und fiel fast aus dem Bett.
Nachdem er geduscht und sich angezogen hatte, verließ er ohne Frühstück das Haus.
Der Appetit war ihm vergangen.
Griesgrämig erwartete er das wassergefüllte Loch auf dem Weg zu seiner Arbeit.
Wütend stampfte er mitten in die Pfütze als er es erreichte und musste so mit nassen Schuhen und Socken ins Büro gehen.

Es war seit langem einer der unproduktivsten Arbeitstage in Herrn Oink von Ploinks Leben.
Er kämpfte mehrmals damit auf dem Schreibtisch einzuschlafen.
Das blieb auch seinen Kollegen nicht verborgen und so schickte ihn sein Vorgesetzter schließlich mittags mit einer Kritischen Abmahnung für Besonders Unangebrachtes Mitarbeiterverhalten (KABUM) nach Hause.

Auch gut, dachte sich Herr Oink von Ploink, dem sowieso gerade nicht nach Arbeiten zumute war.
Zu Hause legte er sich wieder ins Bett und fiel sofort in einen tiefen Schlaf.
Nachmittags wachte er wieder auf und machte sich auf den Weg zu Oma und Herkules.
Er war gespannt, ob die beiden etwas im Amt für Straßenausbesserung erreichen konnten.

Ungeduldig verlagerte er das Gewicht von einem Bein auf das andere als Oma nicht sofort öffnete.
Er klingelte erneut.
Dann hörte er drinnen Schritte.
Die Wohnungstür wurde von einer lächelnden Oma mit Herkules auf dem Arm geöffnet.
Drinnen roch es nach nassem Hund und Hundefutter.

- "Warum denn so ungeduldig, Oskar?
   Und wie kommt es, dass du nicht bei der Arbeit bist?"

Herr Oink von Ploink, dessen Vorname übrigens Oskar war, trat ein und schloss die Wohnungstür hinter sich.
Er legte den Mantel ab und setzte sich auf das alte Sofa, das Oma unbedingt aus ihrere früheren Wohnung mitnehmen musste, weil es so gemütlich war.
Und das stimmte auch – es war tatsächlich sehr gemütlich.

- "Wie lief es bei der Abteilung für Straßenausbesserung?"

fragte er ohne große Umschweife.

- "Wir haben eine Nummer gezogen und eine Weile im Wartezimmer gesessen.
   Herkules saß die ganze Zeit neben mir.
   Er ist ja so ein braver Hund.
   Unverständlich, dass jemand ihn einfach ausgesetzt hat.
   Hast du dich eigentlich erkundigt, ob er vielleicht irgendwo vermisst wird?"
- "Nein, das habe ich vergessen.
   Und was ist passiert als ihr mit Warten fertig wart?"
- "Dann wurden wir zu einem Sachbearbeiter geschickt, der unglaublich beschäftigt war.
   Zuerst hatte ich den Eindruck, dass er mir gar nicht richtig zuhört.
   Dann musste ich ziemlich lange warten, bis er mir geantwortet hat.
   Schrecklich, wie unhöflich die Leute heutzutage geworden sind.
   Früher wäre man einer Dame in meinem Alter mit mehr Respekt begegnet."
- "Und was hat er gesagt?"
- "Er wollte von mir noch einmal die genaue Art des Schadens wissen – als wenn ich ihm das nicht schon längst erzählt hätte.
   Er schrieb sich alles auf und meinte dann, dass sich jemand darum kümmern würde."
- "Hat er denn auch erwähnt, wann das geschieht?
   Es sind schließlich schon fast drei Wochen vergangen."
- "Nein, das hat er nicht."

musste Oma enttäuscht zugeben.
Sie dachte eine Weile nach, dann sagte sie:

- "Und wenn wir uns doch selbst darum kümmern?"

Diesmal lachte Herr Oink von Ploink nicht.
Etwas hatte sich geändert.
Er wollte der Welt zeigen, dass er, Oskar Oink von Ploink, sehr wohl in der Lage war, mit einem Loch im Bürgersteig fertig zu werden.
Niemand konnte ihn aufhalten, dieses Problem selbst zu lösen.
Wenn die Verantwortlichen nicht zur Tat schritten, so würde er den Kampf gegen die Löcher dieser Welt beginnen und nicht eher ruhen bis die Menschheit vor gefährlichen Löchern sicher war.

Also schmiedeten Herr Oink von Ploink, Oma und Herkules zusammen einen Plan, wie sie beim Fachhandel für Straßenbaubedarf eine Absperrung bestellen würden und heimlich am Loch aufstellen.
Oma quiekte vor Vergnügen:

- "Das ist alles so aufregend, Oskar!
   Bestimmt kommen wir dann in die Nachrichten als die geheimnisvollen Absperrungsaufsteller oder so ähnlich."

Herr Oink von Ploink stimmte wild entschlossen mit einem Nicken und einem Brummen zu.
Herkules sah ihn an und legte den Kopf schief.
